import datetime
from kck.lib.kck_updater import KCKUpdater

class SimpleClassBasedUpdater1(KCKUpdater):
    name = "simpleclassbasedupdater1"
    database = "test"
    parameters = [
        {"name": "id", "type": "int", "primary_key": True},
        {"name": "testcol2"}
    ]
    query_template = {
        "update": "update testtbl2 set testcol2 = :testcol2 where id = :id",
        "insert": "insert into testtbl2 (testcol2) values (:testcol2) returning id",
    }
    data_type = "string"
