import datetime

from kck.lib.exceptions import AugmentError
from kck.lib.kck_primer import KCKPrimer


def _append_using_trigger_data(key, current, *args, **kwargs):
    for val_tuple in args:
        id, colval = val_tuple
        if current is None or not current:
            current = [[id, colval]]
        current.append([id, colval])
    return current

def _distill_updates(current, args):
    known_ids = set([ctupl[0] for ctupl in current])
    arg_ids = set([atupl[0] for atupl in args])
    new_ids = arg_ids - known_ids
    updating_ids = arg_ids - new_ids
    values_dict = {}
    for ndx, row in enumerate(current):
        if row[0] not in arg_ids:
            continue
        values_dict[row[0]] = dict(value=row[1], ndx=ndx)
    for id, val in args:
        try:
            values_dict[id]['value'] = val
        except KeyError:
            values_dict[id] = dict(value=val)

    return updating_ids, new_ids, values_dict


def _update_using_trigger_data(key, current, params, *args, **kwargs):
    for row in current:
        if row[0] == params['id']:
            row[1] = args[0]
    return current


def _delete_using_trigger_data(key, current, params, *args, **kwargs):
    newrows = []
    for row in current:
        if row[0] == params['id']:
            continue
        newrows.append(row)
    return newrows


class SimpleAugmentingPrimerTarget(KCKPrimer):

    keys = ['simpleaugmentingprimertarget']
    database = 'test'

    query_template = 'select id, testcol1 from testtbl1'

    augment_functions = {
        'update_row': _update_using_trigger_data,
        'delete_row': _delete_using_trigger_data
    }
