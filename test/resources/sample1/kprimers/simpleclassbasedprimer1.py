import datetime
from kck.lib.kck_primer import KCKPrimer

class SimpleClassBasedPrimer1(KCKPrimer):

    keys = ["simpleclassbasedprimer1"]
    database = "test"
    parameters = [
        {"name": "id", "type": "int"}
    ]
    expire = datetime.timedelta(seconds=2)
    query_template = "select testcol2 from testtbl2 where id = :id"

