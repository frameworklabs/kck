import datetime

from kck.lib.kck_cache import KCKCache
from kck.lib.kck_primer import KCKPrimer


def refresh__every_id__every_two_seconds(primer_obj, row):
    id = row[0]
    cache_obj = KCKCache.get_instance()
    existing_record = cache_obj.get("refreshingclassbased2/{}".format(id))
    if existing_record["modified"] < datetime.datetime.utcnow() - datetime.timedelta(seconds=2):
        cache_obj.refresh("refreshingclassbased2/{}".format(row[0]), queued=True)
    return True


class RefreshingClassBased2(KCKPrimer):

    keys = ["refreshingclassbased2"]
    database = "test"
    parameters = [
        {"name": "id", "type": "int"}
    ]
    # expire = datetime.timedelta(seconds=4)
    query_template = "select testcol2 from testtbl2 where id = :id"

    processes = dict(
        refresh__or_all_ids_in_testtbl1__every_two_hours={
                "database": "test",
                "type": "sql",
                "query": "select id from testtbl2",
                "hooks": {
                    "pre": None,
                    "row": [refresh__every_id__every_two_seconds],
                    "post": None,
                }
            },
        **KCKPrimer.processes
    )


