import datetime

from kck.lib.kck_cache import KCKCache
from kck.lib.kck_primer import KCKPrimer

# cache_obj = KCKCache.get_instance()

# def refresh__every_id__every_two_seconds(row):
#     id = row[0]
#     existing_record = cache_obj.get("refreshingclassbased1/{}".format(id))
#     if existing_record["modified"] < datetime.datetime.utcnow() - datetime.timedelta(seconds=2):
#         cache_obj.refresh("refreshingclassbased1/{}".format(row[0]), queued=True)
#     return True

class RefreshingClassBased1(KCKPrimer):

    keys = ["refreshingclassbased1"]
    database = "test"
    parameters = [
        {"name": "id", "type": "int"}
    ]
    expire = datetime.timedelta(seconds=2)
    query_template = "select testcol2 from testtbl2 where id = :id"

    # refresh = {
    #     "directives": {
    #         "refresh__or_all_ids_in_testtbl1__every_two_hours": {
    #                 "database": "test",
    #                 "type": "sql",
    #                 "query": "select id from testtbl2",
    #                 "process": {
    #                     "pre": None,
    #                     "row": refresh__every_id__every_two_seconds,
    #                     "post": None,
    #                 }
    #             }
    #         }
    #     }

