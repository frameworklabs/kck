import datetime

from kck.lib.kck_primer import KCKPrimer


class SimpleAugmentingPrimerTrigger(KCKPrimer):

    keys = ["simpleaugmentingprimertrigger"]
    database = "test"

    query_template = "select testcol1 from testtbl1 where id = :id"

    on_event_augment = (
        ('update', 'update_row', 'simpleaugmentingprimertarget'),
        ('delete', 'delete_row', 'simpleaugmentingprimertarget'),
    )

    parameters = [
        {"name": "id", "type": "int"}
    ]