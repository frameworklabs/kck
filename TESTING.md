# Testing KCK

## prerequisites
###services
* PostgreSQL
* Cassandra

## set up the database
this is what I do:

1. fire up the PostgreSQL prompt:

    ~~~~
    sudo su - postgres
    psql template1
    ~~~~

2. create and configure the database

    ~~~~
    create database kck_test;
    create user kck_test with password 'kck_test';
    grant all privileges on database 'kck_test' to kck_test;
    ~~~~

## run the tests
1. run py.test:

    ~~~~
    KCK_ENVIRONMENT=test py.test
    ~~~~

2. run the JWT test a slightly different way (because it requires a different config):

    ~~~~
    KCK_ENVIRONMENT=test-jwt py.test tests/test_jwt.py
    ~~~~
