# Installing KCK

## Prerequisites
1. Linux.  *This is probably not actually a prereq, but I'll let someone with another platform update the docs to reflect this.*
2. Git.  You'll need git to retrieve the source from the Framework Labs account on Gitlab.
3. Python 3.  That's what KCK is written in.
4. Virtualenv wrapper.  *This is not exactly true either, but it's what I use, so the docs are going to reflect this.*

## Decide where to install KCK
You'll need to set your $PYTHONPATH environment variable to the directory above where you install KCK.

## *Git* the repository
From the directory where KCK will be installed:
~~~~
git clone git@gitlab.com:frameworklabs/kck.git
~~~~

## Create a virtualenv environment for KCK
~~~~
mkvirtualenv --python=/usr/bin/python3 kck
~~~~

## Make your life easier
Edit ~/.virtualenvs/kck/bin/postactivate and add these lines:
~~~~
ROOTDIR=/home/fred/src/kck
export PYTHONPATH="$ROOTDIR/.."
cd $SRCDIR
~~~~

## Get to work
When you want to hack on KCK or run the tests, you can simply:
~~~~
workon kck
~~~~
