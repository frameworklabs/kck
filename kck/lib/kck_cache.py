import json
import os
import random
import zlib
import pickle
import dateutil.parser
import datetime
import logging

from simple_settings import settings
# from .config import kck_config_lookup
from .exceptions import PrimerComputerReturnedNoResults, KCKKeyNotSetException, KCKKeyNotRegistered, KCKUnknownKey, \
    CanNotAugmentWithoutVersion, CantAugmentUnknownPrimer
from .cassandra_actor import CassandraActor

SCRIPT_DIRPATH = os.path.dirname(os.path.realpath(__file__))
KCK_SOURCES_DIRPATH = os.path.join(SCRIPT_DIRPATH, "..")

process_cache_singleton = None
logger = logging.getLogger(__name__)


class KCKCache(CassandraActor):

    prime_on_cache_miss = False

    def __init__(self,
                 inhibit_framework_registration=False,
                 key_string_sep="/",
                 serialize=True,
                 compress=True,
                 data_obj=None,
                 refresh_selector_string=None):
        """

        :param inhibit_framework_registration:
        :param key_string_sep:
        :param serialize:
        :param compress:
        :param data_obj:

        :param refresh_selector_string:
            the value that is to be assigned to the selector field of refresh queue
            records when they are 'claimed' by the refresh worker

        """
        self.primitive_init()
        if data_obj is None:
            from .kck_data import KCKData
            self.init(
                inhibit_framework_registration=inhibit_framework_registration,
                cache_obj=self,
                data_obj=KCKData.get_instance(cache_obj=self),
                serialize=serialize,
                compress=compress)
        else:
            self.init(
                inhibit_framework_registration=inhibit_framework_registration,
                cache_obj=self,
                data_obj=data_obj,
                serialize=serialize,
                compress=compress)

        self.prime_on_cache_miss = settings.KCK['prime_on_cache_miss']
        self.keysep = key_string_sep

        self.refresh_selector_string = refresh_selector_string

    @classmethod
    def get_instance(cls, **kwargs):
        global process_cache_singleton
        if process_cache_singleton is None or "new_instance" in kwargs and kwargs["new_instance"]:
            if 'new_instance' in kwargs:
                kwargs.pop('new_instance')
            process_cache_singleton = cls(**kwargs)
        return process_cache_singleton

    @staticmethod
    def gen_new_version():
        """returns a new random integer to be used for versioning"""
        minver = -2147483648
        maxver = 2147483647
        return random.randint(minver, maxver)

    def mget(self, key_list, prime_on_cache_miss=False):
        ret = []
        for key in key_list:
            try:
                ret.append(self.get(key, prime_on_cache_miss=prime_on_cache_miss))
            except KCKUnknownKey as e:
                ret.append(
                    dict(
                        success=False,
                        key=key,
                        exception=e
                    )
                )
            except Exception as e:
                logger.debug("kck_cache.KCKCache.mget - shouldn't be getting here...need to add a handled exception")
                raise e
        return ret

    def set(self, key, val, expire=None):
        """places a cache entry in the primary cache with key=key and value=val"""
        self._primcache_put(key, val, expire=expire)
        self._seccache_put(key, val, expire=expire)

        # try to call any augment listeners
        try:
            self._do_on_event_augment(key, 'update', value=val)
        except CantAugmentUnknownPrimer:
            pass

    def get(self, key, prime_on_cache_miss=False):
        """given a key, try to find a corresponding value in the primary cache. failing that,
           try to find the corresponding value in the secondary cache. failing that, if
           self.prime_on_cache_miss is True, call the primer"""

        try:
            ret = self._primcache_get(key)
            return ret

        except KCKKeyNotSetException:

            try:
                self.primer_obj(key)
            except KCKKeyNotRegistered:

                try:
                    ret = self._seccache_get(key)
                    return ret
                except KCKKeyNotSetException:
                    raise KCKUnknownKey(key)

            # if kck is depending on the val cached in the
            # secondary cache, it's time to refresh
            # self.refresh(key, queued=True)

            if not self.prime_on_cache_miss and not prime_on_cache_miss:

                ret = self._seccache_get(key)
                self.refresh(key, queued=True)
                return ret

            try:
                ret = self._seccache_get(key)
                self.refresh(key, queued=True)
                return ret

            except KCKKeyNotSetException:
                try:
                    return self._prime(key)
                except PrimerComputerReturnedNoResults:
                    raise KCKKeyNotSetException(
                        key,
                        self.prim_cache_name,
                        msg="primer found, but compute() returned no results")

    def dump_data_to_file(self, output_filename='/tmp/kck_dump.out'):
        self.raw_cache_dump(tbl=self.sec_cache_name, output_filename=output_filename)

    def load_data_from_file(self, input_filename='/tmp/kck_dump.out'):
        self.raw_cache_load(tbl=self.sec_cache_name, input_filename=input_filename)

    def purge(self, key, version=None):

        if version is None:
            version = self.get(key)["version"]
            return self.purge(key, version)

        self._do_on_event_augment(key, 'delete', version=version)

        self._cache_delete(key, self.prim_cache_name, version=version)
        self._cache_delete(key, self.sec_cache_name, version=version)
        return True

    def _do_on_event_augment(self, key, trigger_event_type, value=None):
        """call an augment function for the primer instance associated
                with the key and trigger_event_type"""
        def key_from_tmpl(tmpl, param_dict):

            def elemval(s):
                if s[0] == ":":
                    return param_dict[s[1:]]
                return s

            if '/' not in tmpl:
                return elemval(tmpl)
            key_list = []
            targetkey_elements = tmpl.split(self.keysep) if tmpl is not None else None
            for key_element in targetkey_elements:
                key_list.append(elemval(key_element))
            return self.keysep.join([str(s) for s in key_list])

        # get the primer object for the key and define the param dict
        try:
            primer_obj = self.primer_obj(key)
        except KCKKeyNotRegistered:
            raise CantAugmentUnknownPrimer
        param_dict = primer_obj.key_to_param_dict(key)

        # bail if the primer object does not define on_event_augment
        if not hasattr(primer_obj, 'on_event_augment'):
            return CantAugmentUnknownPrimer

        # for each entry in the on_event_augment list, check if the event type matches the trigger
        #  event type in the parameters and, if so, queue a refresh request with a hint
        for event_type, target_augment_function_handle, target_keytmpl in primer_obj.on_event_augment:
            if event_type != trigger_event_type:
                continue

            # get the refresh key that holds the parameter information for the
            # augment operation on the target key
            refresh_key = key_from_tmpl(target_keytmpl, param_dict)

            self.refresh(refresh_key,
                         hints=[{"modified": datetime.datetime.utcnow(),
                                 "augmentor": target_augment_function_handle,
                                 "params": param_dict,
                                 "args": [value]}],
                         queued=True)

    def refresh(self, key,
                hints=None, queued=False,
                version=None, force_prime=False):

        # queued request, update modified field in hints, queue request and return
        if bool(queued):
            return self.raw_queue_refresh(key=key, hints=hints, version=version)

        # get cache entry or, if undefined, prime
        just_primed = False
        current_version = version
        primer_obj = self.primer_obj(key)
        if version is None:
            try:
                cache_entry_record = self.get(key)
            except KCKKeyNotSetException:
                cache_entry_record = self._prime(key)
                just_primed = True
            current_version = cache_entry_record['version']

        # do pre-refresh hooks
        primer_obj.do_hooks("pre_refresh", key=key, hints=cache_entry_record)


        # apply hints
        if hints and bool(hints[0]):

            primer_obj.do_hooks('pre_augment', key=key, hints=cache_entry_record)

            val_current_thru = cache_entry_record['modified']
            raw_val = cache_entry_record['value']

            for hint_list_as_json in hints:

                # lower level seems pretty insistent on sending a list
                # with a single element, null
                if not hint_list_as_json:
                    continue

                hint_list = self._decode_json(hint_list_as_json)
                for hint_as_json in hint_list:

                    hint = self._decode_json(hint_as_json) if type(hint_as_json) is str else hint_as_json

                    if type(hint['modified']) is str:
                        hint['modified'] = dateutil.parser.parse(hint['modified'])

                    # skip any hints that *may* have already been included
                    # in the primed value, but if we skip anything, request
                    # a hint-less refresh
                    if hint['modified'] < val_current_thru:
                        self.refresh(key, queued=True)
                        continue

                    augment_function_handle = hint['augmentor']
                    raw_val = primer_obj.augment(
                        augment_function_handle, key, raw_val, hint['params'],
                        *hint['args'] if 'args' in hint else [],
                        **hint['kwargs'] if 'kwargs' in hint else {})

            self._do_on_event_augment(key, 'update', value=raw_val)

            cache_entry_record = self._primcache_put(key, raw_val, primer_obj.expire,
                                                     check_last_version=True,
                                                     version=current_version)
            self._seccache_put(key, raw_val, primer_obj.expire)

            primer_obj.do_hooks('post_augment', key=key, hints=cache_entry_record)

        # no hints and didn't just prime, so prime
        elif not just_primed:
            cache_entry_record = self._prime(key)


        primer_obj.do_hooks("post_refresh", key=key, hints=cache_entry_record)

        return cache_entry_record

    def primer_registered(self, name):
        return bool(name in self.primers)

    def register_primer(self, name, primer_obj):
        logger.info('registering primer: %s', name)
        primer_obj.set_cache(self)
        primer_obj.set_data(self.data_obj)
        primer_obj.set_name(name)
        self.primers[name] = primer_obj

    def _prime(self, key):

        try:
            primer_obj = self.primer_obj(key)

            primer_obj.do_hooks("pre_prime", key=key)

            # note time just prior to calling compute()
            effective_timestamp = datetime.datetime.utcnow()

            primed_val = primer_obj.compute(key)

        except KCKKeyNotRegistered:
            raise KCKUnknownKey(key)

        # purge refresh requests that came in before the effective timestamp

        self._do_on_event_augment(key, 'update', value=primed_val)

        ret = self._primcache_put(key, primed_val, primer_obj.expire,
                                  check_last_version=True, version=None)
        self._seccache_put(key, primed_val, primer_obj.expire)

        primer_obj.do_hooks("post_prime", key=key, hints=ret)

        return ret

    def _primcache_get(self, key):
        return self._cache_get(key, self.prim_cache_name)

    def _seccache_get(self, key, exc_class=None):
        return self._cache_get(key, self.sec_cache_name, exc_class)

    def _primcache_put(self, key, val,
                       expire=None, version=None, check_last_version=None):
        return self._cache_put(key, self.prim_cache_name, val,
                               expire=expire, version=version,
                               check_last_version=check_last_version)

    def _seccache_put(self, key, val,
                      expire=None, version=None, check_last_version=None):
        return self._cache_put(key, self.sec_cache_name, val,
                               expire=expire, version=version,
                               check_last_version=check_last_version)

    def _cache_get(self, key, tbl, exc_class=None):

        exc_class = exc_class or KCKKeyNotSetException

        raw_get_result = self.raw_cache_get(key, tbl)

        try:
            return self.build_cache_entry_dict(
                key=key,
                version=raw_get_result[0][1],
                value=self.decomprickle(raw_get_result[0][0]),
                tbl=tbl,
                modified=raw_get_result[0][2]
            )

            # return dict(success=True, version=raw_get_result[0][1], key=key,
            #             value=self.decomprickle(raw_get_result[0][0]), tbl=tbl,
            #             modified=raw_get_result[0][2])
        except IndexError:
            raise exc_class(key, tbl)

    def _cache_put(self, key, tbl, val,
                   version=None, check_last_version=None, expire=None):


        new_version = version if version is not None else self.gen_new_version(
        )

        newval = self.comprickle(val)

        current_timestamp = datetime.datetime.utcnow()

        self.raw_cache_put(key, tbl, newval,
                           version=new_version, check_last_version=check_last_version,
                           expire=expire, modified=current_timestamp)
        ret = dict(success=True, version=new_version, key=key, value=val,
                   tbl=tbl, modified=current_timestamp)
        return ret

    def _cache_delete(self, key, tbl):
        self.raw_cache_delete(key, tbl)
        return dict(success=True, key=key, tbl=tbl)

    @classmethod
    def build_cache_entry_dict(cls, key, version, value, tbl, modified):
        return dict(
            success=True,
            version=version,
            key=key,
            value=value,
            tbl=tbl,
            modified=modified)
