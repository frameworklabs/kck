from kck.lib.exceptions import KCKKeyNotSetException


class HintsManager(object):

    _hints = {}
    _purged = {}
    _cache_obj = None

    def __init__(self, cache_obj):
        self._cache_obj = cache_obj

    def set_hint_key(self, cache_entry=None):
        self._hints[cache_entry['key']] = cache_entry

    def unset_hint_key(self, key=None):
        self._purged[key] = True

    def get(self, key, prime_on_cache_miss=False):
        if key in self._purged:

            # try to lookup the primer, raises KCKKeyNotRegistered if unknown key type
            self._cache_obj.primer_obj(key)

            # if we got here, it's known but not set
            raise KCKKeyNotSetException(key, 'purging')

        try:
            return self._hints[key]
        except KeyError:
            return self._cache_obj.get(key)

    def to_dict(self):
        return {'hints': self._hints, 'purged': self._purged}

    def from_dict(self, d):
        if 'hints' in d:
            self._hints = d['hints']
        if 'purged' in d:
            self._purged = d['purged']
