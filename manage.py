#!/usr/bin/env python

import sys
import six
import subprocess
import argparse
import os

from os.path import dirname, join

sys.path.append(join(dirname(__file__), '..'))

import tornado.ioloop
from tornado.options import define, options, parse_command_line

from kck.lib.http_service import KCKHTTPServer

#define("port", default=6001, help="run on the given port", type=int)
# define("debug", default=True, help="run in debug mode")

SCRIPT_DIRPATH = os.path.dirname(os.path.realpath(__file__))
TESTS_DIRPATH = os.path.join(SCRIPT_DIRPATH, "tests")

# --- handle command-line opts
parser = argparse.ArgumentParser(
    description='run KCK command',
    prog='manage.py'
)

progargs = (
    (
        "command",
        "positional",
        "the KCK command to be executed [runhttpserver|runrefreshserver|runupdateserver|runrefreshworker|runupdateworker|runalltests]"
    ),
)

for arginfo in progargs:
    optname, opttype, opthelp = arginfo
    if opttype == "positional":
        parser.add_argument(optname, help=opthelp)
    elif opttype == "short":
        parser.add_argument("--{}".format(optname), help=opthelp, action="store_true")
    elif opttype == "full":
        parser.add_argument("--{}".format(optname), help=opthelp)

args = vars(parser.parse_args())
cmd_opts = {}
for opt, val in six.iteritems(args):
    cmd_opts[opt] = val

def runhttpserver():
    parse_command_line()
    app = KCKHTTPServer.get_instance()
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()

def runalltests():
    envs = {
        "test_jwt.py": {"KCK_ENVIRONMENT": "test-jwt"},
        "test_refresh.py": {"KCK_ENVIRONMENT": "test-refresh"},
        "test_refresh_over_domain.py": {"KCK_ENVIRONMENT": "test-refresh"},
        "DEFAULT": {"KCK_ENVIRONMENT": "test"},
    }
    os.chdir("tests")
    for filename in os.listdir(TESTS_DIRPATH):
        if filename.endswith(".py"):

            basefilename = os.path.basename(filename)
            env_dict = envs[basefilename] if basefilename in envs else envs["DEFAULT"]
            env_dict.update({"PATH": os.environ["PATH"], "PYTHONPATH": os.environ["PYTHONPATH"]})
            subprocess.call(["py.test", filename], env=env_dict)

def runrefreshserver():
    """
    schedules refreshes
    """

    # queue time-based refreshes

def runrefreshworker():
    """
    execute refresh operations
    """

    # run a celery worker that runs refresh operations

def runupdateworker():
    """
    execute update operations
    """

    # run a celery worker that runs update operations
    pass

command_map = {
    "runhttpserver": runhttpserver,
    "runalltests": runalltests,
    "runrefreshserver": runrefreshserver,
    "runupdateworker": runupdateworker,
    "runrefreshworker": runrefreshworker,
}

if cmd_opts["command"] in command_map:
    command_map[cmd_opts["command"]]()