# Status of KCK Project

## 11/16/2017
* added pip packaging info

## 10/10/2017
* queued refresh table and foundations for refresh daemon in place with all tests
  passing again

## 9/13/2017
* adds query type designation that allows for queries to return as lists of dicts
* implements refresh-over-domain
* there's now a manage.py to run the http server and to run all the tests

## 8/30/2017 (2)
* completes JWT implementation

## 8/30/2017 (1)
* implements error handling decorator for all routes in http service
* implements a auth-checking decorator for nearly all routes in http service
* implements login with jwt token return

## 8/29/2017
* implemented result filtering via JSONPath

## 8/16/2017
* implemented a 'processes' feature by which primers can define processes to be periodically run
* implemented a time-based refresh (tests/test_timebased_refresh.py) using process definitions

## 8/14/2017
* 'modified' field is now stored in cache table (Cassandra) and returned from get(). *This is step #1 toward the goal of time-based data refreshes.*

## 8/12/2017
* Basic caching with primes is working for both YAML and Module primer types
* fetch/create/update working via HTTP api
* prime-on-prime and prime-on-update dependency refresh is working
* key expiry is working