import os

SIMPLE_SETTINGS = {
    'OVERRIDE_BY_ENV': True,
    'CONFIGURE_LOGGING': True,
    # 'REQUIRED_SETTINGS': ('API_TOKEN', 'DB_USER'),
    # 'DYNAMIC_SETTINGS': {
    #     'backend': 'redis',
    #     'pattern': 'DYNAMIC_*',
    #     'auto_casting': True,
    #     'prefix': 'MYAPP_'
    # }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s %(levelname)s %(name)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
            'stream': 'ext://sys.stdout'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True
        },
        'cassandra': {
            'level': 'WARNING',
            'handlers': ['console'],
            'propagate': True
        },
        'kck': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True
        },
    }
}

KCK = {
    'environment': os.environ.get('KCK_ENVIRONMENT', 'test'),
    'use_auth': False,
    'prime_on_cache_miss': False,
    'primers_dirpath': '/usr/src/app/test/resources/sample1/kprimers',
    'updaters_dirpath': '/usr/src/app/test/resources/sample1/kupdaters',
    'cassandra': {
        'hosts': ['cassandra'],
        'keyspace': 'kck',
        'tables': {
            'primary_cache': 'test__kck_pri_cache',
            'secondary_cache': 'test__kck_sec_cache',
            'queued_updates': 'test__queued_updates',
            'queued_refreshes': 'test__queued_refreshes',
            'queued_refreshes_counter': 'test__queued_refreshes_counter'
        }
    },
    'databases': {
        'test': {
            'connection_url': 'postgresql://kck_test:kck_test@database/kck_test'
        }
    }
}