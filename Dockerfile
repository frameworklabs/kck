FROM python:3.5

# add what's needed for pip and node pkg install
WORKDIR /usr/src/app
RUN install -d /usr/src/app/requirements
ADD requirements/*.txt /usr/src/app/requirements/

# install system packages
RUN apt-get -y update
RUN apt-get -y install apt-transport-https ca-certificates bash postgresql-client libpython3-dev build-essential libyaml-dev
RUN apt-get -y clean

# install pip packages
RUN CFLAGS= pip install --upgrade pip
RUN pip install psycopg2 --no-cache-dir
RUN CFLAGS= pip install -r requirements/base.txt
RUN CFLAGS= pip install -r requirements/test.txt
#RUN apt-get -y update
#RUN apt-get -y install apt-transport-https ca-certificates
#RUN curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
#RUN sh -c "echo deb https://deb.nodesource.com/node_10.x bionic main > /etc/apt/sources.list.d/nodesource.list"

# INSTALL APP SOURCE
WORKDIR /usr/src/app
RUN cd /usr/src/app
COPY . .

WORKDIR /usr/src/app
#CMD "provision/docker/run_webserver.sh"
#ENTRYPOINT ["provision/docker/entrypoint.sh"]
#EXPOSE 3000
