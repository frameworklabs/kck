# KCK Todo
* implement ability to augment data with hints for class-based primers
    * I think maybe just a augment method might do it
    * the refresh algorithm will call augment when the refresh count equals the size of the hints list
* implement simple daemon for refresh worker
    * handles:
        * time-based refreshes
        * dependency refreshes
* implement proper handling of queued refreshes, ensuring that queue pops are happening, ensuring proper handling of multiple workers vying for the same refreshes (use Cass. versioning and only do work after updating the queue but ensure that, if the refreshes don't occur within a given timeframe, that they are still handled by something one way or another), etc.
* make new queued refreshes incremement counters instead of creating new refresh entries [DONE]
    * this will be used in the selection process when the proper refresh worker is implemented
* implement queued updates
* implement multiple get/put
* implement pattern-based get
* because it would be good to be able to leverage YAML's ability to abbreviate (thus DRYing up config), it'd be better if multiple distinct machine configs could exist in a single YAML.  So maybe have a KCK_ENVIRONMENT and a KCK_MODE environment variable that directs the config module toward a particular section in the config.
* implement a way to spec stub data for use in testing and prototypes
    * this would be cool as a separate .yml that could be added even if the primer is python-based
* add docstrings in cassandra_actor.py and framework_actor.py
